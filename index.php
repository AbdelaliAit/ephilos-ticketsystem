<!DOCTYPE html>
<html lang="en">
<head>
	<title>TicketSystem</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				
				<div class="wrap-ticket100-form-btn">
						<div class="ticket100-form-bgbtn"></div>
						<a class="ticket100-form-btn" href="ticket.php">
							<span>
								Neue Ticket erstellen
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
						</a>
					
				</div>

				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">ID</th>
		                        <th class="column2">Code</th>
		                        <th class="column3">Absender</th>
		                        <th class="column5">Titel</th>
		                        <th class="column6">Dringlichkeit</th>
		                        <th class="column7">Veröffentlicht</th>
		                        <th class="column8">Aktion</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							 	require_once "config.php";
								$sql = "SELECT * FROM tickets ORDER BY ID DESC";
		                    	if($result = mysqli_query($link, $sql))
		                    	{
		                        	if(mysqli_num_rows($result) > 0)
		                        	{
		                        		while($row = mysqli_fetch_array($result))
		                        		{

				                          if($row['dringlichkeit']=="Niedrig")
				                          {
				                              $dringlichkeit="<span class='badge badge--info badge--small'>Niedrig</span>";
				                          
				                          }if($row['dringlichkeit']=="Mittel")
				                          {
				                              $dringlichkeit="<span class='badge badge--warning badge--small'>Mittel</span>";
				                          }
				                          if($row['dringlichkeit']=="Hoehe")
				                          {
				                              $dringlichkeit="<span class='badge badge--danger badge--small'>Höhe</span>";
				                          }
							?>

											<tr>
												<td class="column1"><?php echo $row['id'] ?></td>
												<td class="column2"><?php echo $row['code'] ?></td>
												<td class="column3"><?php echo $row['email_absender'] ?></td>
												<td class="column5"><?php echo $row['titel'] ?></td>
												<td class="column6"><center><?php echo $dringlichkeit;?></center></td>
												<td class="column7"><?php echo date('Y-m-d',strtotime($row['created'] )); ?></td>
												<td class="column8">
						                           <a class="badge badge--primary mb-2" href="ticket-details.php?id=<?php echo $row['id'];?>" style="color: white;cursor: pointer;" title="bearbeiten"
						                              href="javascript:void(0)">
						                              <i class="fa fa-eye" style="font-size: 15px;"> </i>
						                           </a>
						                        </td>
											</tr>
							<?php 
									    }
									}
								}

								// Close connection
	    						mysqli_close($link);
							?>
								
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	

	<script src="js/main.js"></script>
</body>
</html>