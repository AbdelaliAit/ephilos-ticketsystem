<!DOCTYPE html>
<html lang="en">
<head>
	<title>TicketSystem</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

	<link rel="stylesheet" type="text/css" href="css/card.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				
				<div class="wrap-ticket100-form-btn">
						<div class="ticket100-form-bgbtn"></div>
						<a class="ticket100-form-btn" href="index.php">
							<span>
								Alle Tickets
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
						</a>
					
				</div>

				<?php 
							 	require_once "config.php";
							 
								$id = $_GET['id'];
								$id = mysqli_real_escape_string($link,$id);
								$sql = "SELECT * FROM `tickets` WHERE `id`='" . $id . "' LIMIT 1";

								if($result = mysqli_query($link, $sql))
		                    	{
		                        	if(mysqli_num_rows($result) > 0)
		                        	{
		                        		$row = mysqli_fetch_array($result);

		                        		if($row['dringlichkeit']=="Niedrig")
				                          {
				                              $dringlichkeit="<span class='badge badge--info badge--small'>Niedrig</span>";
				                          
				                          }if($row['dringlichkeit']=="Mittel")
				                          {
				                              $dringlichkeit="<span class='badge badge--warning badge--small'>Mittel</span>";
				                          }
				                          if($row['dringlichkeit']=="Hoehe")
				                          {
				                              $dringlichkeit="<span class='badge badge--danger badge--small'>Höhe</span>";
				                          }
		                        	?>
		                        		<div class="table100">
												<div class="blog-card spring-fever">
												  <div class="title-content">
												    <h3><a href="#"><?php echo $row['titel'] ?></a></h3>
												    <div class="intro"> <a href="#"><?php echo $dringlichkeit ?></a> </div>
												  </div>
												  <div class="card-info">
												   <?php echo $row['inhalt'] ?>
												  </div>
												  <div class="utility-info">
												    <ul class="utility-list">
												      <li><span class="licon icon-dat"></span><?php echo $row['created'] ?></li>
												      <li><span class="licon icon-user"></span><a href="#"><?php echo $row['email_absender'] ?></a></li>
												    </ul>
												  </div>
												  <div class="gradient-overlay"></div>
												  <div class="color-overlay"></div>
												</div><!-- /.blog-card -->
										</div>
		                       	<?php
		                        		
		                        	}

		                        }
								
				?>
				
			</div>
		</div>
	</div>
	

	<script src="js/main.js"></script>
</body>
</html>