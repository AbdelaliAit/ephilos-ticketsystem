
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Erstellung des Tickets</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
<?php
	// Include config file
	require_once "config.php";
	 
	// Define variables and initialize with empty values
	$dringlichkeit = $titel = $email_absender = $inhalt = "";
	$dringlichkeit_err = $titel_err = $email_absender_err = $inhalt_err = "";
	

	// Processing form data when form is submitted
	if($_SERVER["REQUEST_METHOD"] == "POST"){
	    
	    $code = generateRandomString(6);
	    $dringlichkeit =$_POST["dringlichkeit"] ;
	    $titel = mysqli_real_escape_string($link,$_POST["titel"]);
	    $email_absender = $_POST["email_absender"];
	    $inhalt =  mysqli_real_escape_string($link,$_POST["inhalt"]);
        $created=date("Y-m-d H:i:s");
	        

		$dringlichkeit_arr = array('Niedrig', 'Mittel', 'Hoehe');
		if (!in_array($dringlichkeit , $dringlichkeit_arr)) {
		      $err = "Bitte wählen Sie eine Möglichkeit (Niedrig / Mittel / Höhe) ";
		    
		}

		 // Validate Titel
	    if(empty($titel) ){
	        $err = "Bitte geben Sie einen gültigen Titel ein.";
	       
	    } elseif(!preg_match('/[^A-Za-z0-9\.\/\\\\]|\..*\.|\.$/', $titel)){
	        $err = "Bitte geben Sie einen gültigen Titel ein.";
	       
	    } 


		if(!filter_var($email_absender, FILTER_VALIDATE_EMAIL) || empty($email_absender) ) {
		     //Valid email!
			 $err = "Sie haben eine ungültige E-Mail eingegeben. Bitte versuche es erneut";
			
		}

	   
	    
	    if(empty($inhalt)){
	         $err = "Bitte geben Sie einen Inhalt ein.";   
	         
	    } 

	 
	    
	    // Check input errors before inserting in database
	    if(empty($err)){
	        // Prepare an insert statement
	        

	      
			
			$sql = "INSERT INTO tickets (code, email_absender, titel, inhalt, created, dringlichkeit) VALUES ('$code', '$email_absender', '$titel','$inhalt', '$created', '$dringlichkeit')";


			if(mysqli_query($link, $sql)){
			    // Records created successfully. Redirect to landing page
	            header("location: index.php");
			} else{
				$err_msg= "etwas ist schief gelaufen, bitte versuchen Sie es später noch einmal !".mysqli_error($link);
			}
	        
	    }
	    
	    // Close connection
	    mysqli_close($link);
	}
?>


	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				
				

				<div class="table100">
					<div class="wrap-ticket100-form-btn">
							<div class="ticket100-form-bgbtn"></div>
							<a class="ticket100-form-btn" href="index.php">
								<span>
									Alle Tickets
									<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
								</span>
							</a>
						
					</div>

					<div class="container-contact100">
							<div class="wrap-contact100">
								


								<form class="contact100-form validate-form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
									<input type="hidden" name="act" value="insert">
									<span class="contact100-form-title">
										Neue Ticket erstellen
									</span>

									<div class="wrap-input100 input100-select">
										<span class="label-input100">Dringlichkeit :</span>
										<div>
											<select class="selection-2 select-css" name="dringlichkeit" id="dringlichkeit"  >
												<option <?php echo ($_POST['dringlichkeit']=="Niedrig") ?"selected='selected'":"" ?> value="Niedrig">Niedrig</option>
					                            <option <?php echo ($_POST['dringlichkeit']=="Mittel") ?"selected='selected'":"" ?> value="Mittel">Mittel</option>
					                            <option <?php echo ($_POST['dringlichkeit']=="Hoehe") ?"selected='selected'":"" ?>value="Hoehe">Hoehe</option>
											</select>
										</div>
										<span class="focus-input100"></span>
									</div>


									<div class="wrap-input100 validate-input" data-validate="Titel ist erforderlich">
										<span class="label-input100">Titel</span>
										<input class="input100" type="text" placeholder="Titel" id="titel" name="titel"  value="<?php echo $_POST['titel'] ?>">
										<span class="focus-input100"></span>
									</div>

									<div class="wrap-input100 validate-input" data-validate = "Eine gültige E-Mail-Adresse ist erforderlich: ex@abc.xyz">
										<span class="label-input100">Email der Absender</span>
										<input class="input100" type="text" name="email_absender" placeholder="Enter your email addess" value="<?php echo $_POST['email_absender'] ?>">
										<span class="focus-input100"></span>
									</div>


									<div class="wrap-input100 validate-input" data-validate = "Inhalt ist erforderlich">
										<span class="label-input100">Inhalt</span>
										<textarea class="input100"name="inhalt" id="inhalt"><?php echo $_POST['inhalt'] ?></textarea>
										<span class="focus-input100"></span>
									</div>

									<?php 
										if( $err!=""){
									?>
									<div class="error"><?php echo $err ?></div>
									<?php 
									}
									?>


									<?php 
										if( $err_msg!=""){
									?>
									<div class="error"><?php echo $err_msg ?></div>
									<?php 
									}
									?>

									<center>
										<div class="container-speichern-form-btn">
											<div class="wrap-speichern-form-btn">
												<div class="speichern-form-bgbtn"></div>
												<button class="speichern-form-btn">
													<span>
														speichern
														<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
													</span>
												</button>
											</div>
										</div>
									</center>
									
									
								</form>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	


	<script src="js/main.js"></script>



</body>
</html>
